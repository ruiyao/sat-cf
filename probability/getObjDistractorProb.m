function [P_D] = getObjDistractorProb(patch, distractor_hist, distractor_fg_hist, n_bins, grayscale_sequence)
%% computes pixel-wise probabilities (PwP) given PATCH and models DISTRACTOR_HIST and DISTRACTOR_FG_HIST

% check whether the patch has 3 channels
[h, w, d] = size(patch);
% figure out which bin each pixel falls into
bin_width = 256/n_bins;
% convert image to d channels array
patch_array = reshape(double(patch), w*h, d);
% to which bin each pixel (for all d channels) belongs to
bin_indices = floor(patch_array/bin_width) + 1;
% Get pixel-wise posteriors (PwP)
P_distractor = getP(distractor_hist, h, w, bin_indices, grayscale_sequence);
P_distractor_fg = getP(distractor_fg_hist, h, w, bin_indices, grayscale_sequence);

% Object-likelihood map
P_D = P_distractor_fg ./ (P_distractor_fg + P_distractor);

end