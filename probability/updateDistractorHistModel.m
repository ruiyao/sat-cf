function [distractor_hist_new, obj_hist_new] = updateDistractorHistModel(is_new_model, patch, distractors, obj_rect, norm_area, n_bins, grayscale_sequence, distractor_hist, obj_hist, learning_rate_pwp)
%UPDATEHISTMODEL create new models for foreground and background or update the current ones

%% Get BG (frame around target_sz) 
sz = size(patch);
sz = sz(1:2);

% todo: test and add
distractors_mask = false(sz);
num_distr = size(distractors,1);
for i = 1:num_distr
	distractors_mask(distractors(i,2):distractors(i,2)+distractors(i,4),distractors(i,1):distractors(i,1)+distractors(i,3)) = true;
end

%% Get FG masks (inner portion of target_sz)
obj_mask = false(sz);
obj_mask(obj_rect(2):obj_rect(2)+obj_rect(4),obj_rect(1):obj_rect(1)+obj_rect(3)) = true;

% pad_offset2 = (bg_area-fg_area)/2; % we constrained the difference to be mod2, so we do not have to round here
% assert(sum(pad_offset2==round(pad_offset2))==2, 'difference between bg_area and fg_area has to be even.');
% fg_mask = false(bg_area); % init fg_mask
% pad_offset2(pad_offset2<=0)=1;
% fg_mask(pad_offset2(1)+1:end-pad_offset2(1), pad_offset2(2)+1:end-pad_offset2(2)) = true;

%% Resize mask of distractors and object, image patch
obj_mask = mexResize(obj_mask, norm_area, 'auto');
distractors_mask = mexResize(distractors_mask, norm_area, 'auto');
patch = mexResize(patch, norm_area, 'auto');

%% (TRAIN) BUILD THE MODEL
if is_new_model
    % from scratch (frame=1)
    distractor_hist_new = computeHistogram(patch, distractors_mask, n_bins, grayscale_sequence);
    obj_hist_new = computeHistogram(patch, obj_mask, n_bins, grayscale_sequence);
else
    % update the model
    distractor_hist_new = (1 - learning_rate_pwp)*distractor_hist + learning_rate_pwp*computeHistogram(patch, distractors_mask, n_bins, grayscale_sequence);
    obj_hist_new = (1 - learning_rate_pwp)*obj_hist + learning_rate_pwp*computeHistogram(patch, obj_mask, n_bins, grayscale_sequence);
end

end