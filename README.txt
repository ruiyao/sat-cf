Semantics-Aware Tracker (v1.0)

This code accompanies the paper:
Rui Yao, Guosheng Lin, Chunhua Shen, Yanning Zhang, Qinfeng Shi. 
Semantics-Aware Visual Object Tracking.
IEEE Transactions on Circuits and Systems for Video Technology, 2018.

Contact: Rui Yao (ruiyao@cumt.edu.cn)

------------
License
------------

  THIS SOFTWARE IS PROVIDED BY LU ZHANG AND AURENS VAN DER MAATEN ''AS IS'' AND ANY EXPRESS
  OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO 
  EVENT SHALL LU ZHANG AND LAURENS VAN DER MAATEN BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, 
  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
  OF SUCH DAMAGE.

------------
Citations
------------

In case you use SAT-CF code in your work, please cite the following paper:

@article{YaoSemantics,
  title={Semantics-Aware Visual Object Tracking},
  author={Yao, Rui and Lin, Guosheng and Shen, Chunhua and Zhang, Yanning and Shi, Qinfeng},
  journal={IEEE Transactions on Circuits & Systems for Video Technology},
  volume={PP},
  number={99},
  pages={1-1},
}

------------
Requirements
------------

This code has been developed and tested with Ubuntu 16.04, Matlab R2017a (64-bit).
1. matconvnet-1.0-beta23

-----
Usage
-----

>> demo
