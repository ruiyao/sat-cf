function [results] = trackerMain(base_path, p, im, bg_area, fg_area, area_resize_factor, cfg, svm_distractor_w)
%TRACKERMAIN contains the main loop of the tracker, P contains all the parameters set in runTracker
%% INITIALIZATION
num_frames = numel(p.img_files);
% used for OTB-13 benchmark
OTB_rect_positions = zeros(num_frames, 4);
pos = p.init_pos;
target_sz = p.target_sz;
num_frames = numel(p.img_files);
% patch of the target + padding
patch_padded = getSubwindow(im, pos, p.norm_bg_area, bg_area);
% initialize hist model
is_new_pwp_model = true;
[bg_hist, fg_hist] = updateHistModel(is_new_pwp_model, patch_padded, bg_area, fg_area, target_sz, p.norm_bg_area, p.n_bins, p.grayscale_sequence);

grabCutParam = loadGrabCutParam;
target_color_score = 0;

is_save_visual_result = 0;

if strcmp(p.dataset, 'demo')
    semantic_folder = [base_path '/' p.seq_name '/predict_result_scoremap_imgname'];
else
    semantic_folder = [p.semantic_path '/predict_result_' p.dataset '_' p.seq_name '_scoremap_imgname'];
end
semantic.layers = [-3];
semantic.mean_scores = [];

% initialization. Object vs surrounding, original image size
target_pos_dat = pos([2,1]);
target_sz_dat = target_sz([2,1]);

% search_sz = [];
% Store current location
state.target_pos_history = target_pos_dat;
state.target_sz_history = target_sz_dat;
distractor_hist = bg_hist;
obj_hist = fg_hist;
hypotheses = [];
is_new_dist_model = 1;

% original_response_cf = [];

is_new_pwp_model = false;
% Hann (cosine) window
if isToolboxAvailable('Signal Processing Toolbox')
    hann_window = single(hann(p.cf_response_size(1)) * hann(p.cf_response_size(2))');
else
    hann_window = single(myHann(p.cf_response_size(1)) * myHann(p.cf_response_size(2))');
end
% gaussian-shaped desired response, centred in (1,1)
% bandwidth proportional to target size
output_sigma = sqrt(prod(p.norm_target_sz)) * p.output_sigma_factor / p.hog_cell_size;
y = gaussianResponse(p.cf_response_size, output_sigma);
yf = fft2(y);

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%
t_imread = 0;
%% MAIN LOOP
tic;
for frame = 1:num_frames
    search_sz   = floor(target_sz_dat + cfg.search_win_padding*max(target_sz_dat));
    if frame>1
        tic_imread = tic;
        im = imread([p.img_path p.img_files{frame}]);
        t_imread = t_imread + toc(tic_imread);
        
        if ~ismatrix(im)
            img = rgb2gray(im);
        else
            img = im;
        end
        
        %% TESTING step
        % extract patch of size bg_area and resize to norm_bg_area
        im_patch_cf = getSubwindow(im, pos, p.norm_bg_area, bg_area);
        pwp_search_area = round(p.norm_pwp_search_area / area_resize_factor);
        % extract patch of size pwp_search_area and resize to norm_pwp_search_area
        [im_patch_pwp, im_patch_pwp_rect] = getSubwindow(im, pos, p.norm_pwp_search_area, pwp_search_area);
        
        % compute feature map
        zt = getFeatureMap(im_patch_cf, p.feature_type, p.cf_response_size, p.hog_cell_size);
        % apply Hann window
        zt = bsxfun(@times, zt, hann_window);
        % compute FFT
        zf = fft2(zt);
        
        kzf = gaussian_correlation(zf, model_xf, p.sigma);
        response_cf = real(ifft2(model_alphaf .* kzf));  % equation for fast detection
        
        %%
        % Crop square search region (in feature pixels).
        response_cf = cropFilterResponse(response_cf, floor_odd(p.norm_delta_area / p.hog_cell_size));
        original_response_cf = response_cf; % for segmentation in scale estimation
        
        if p.hog_cell_size > 1
            % Scale up to match center likelihood resolution.
            response_cf = mexResize(response_cf, p.norm_delta_area,'auto');
        end
        
        [~,name,~] = fileparts(p.img_files{frame});
        s_scoremap = load(fullfile(semantic_folder, [name '.mat']));
        %     s_scoremap.data_obj.img_size = size(im);
        [semantic_map, patch_padded_semantic, semantic] = getSemanticSubwindow(s_scoremap, im, pos, p.norm_bg_area, bg_area, target_sz, semantic);
        
        im_semantic = semantic_map;
        im_patch_semantic = getSubwindow(semantic_map, pos, p.norm_pwp_search_area, pwp_search_area);

        %% ESTIMATION
        if cfg.distractor_aware
            prev_pos = state.target_pos_history(end,:); % Original image size
            prev_sz = state.target_sz_history(end,:);
            
            % Previous object location (possibly downscaled)
            target_pos_dat = prev_pos;
            target_sz_dat = prev_sz;
            
            % Search region
            search_rect = pos2rect(target_pos_dat, search_sz);
            [search_win_semantic, padded_search_win_semantic] = getSubwindowMasked(im_semantic, target_pos_dat, search_sz);
            [search_win, padded_search_win] = getSubwindowMasked(im, target_pos_dat, search_sz);
            search_win(padded_search_win) = 0;
            
            pm_search_semantic = mat2gray(search_win_semantic);
            pm_search_semantic(padded_search_win_semantic) = 0;
            
            % Cosine/Hanning window
            cos_win = hann(search_sz(2)) * hann(search_sz(1))';
            
            % Localize
            [hypotheses, vote_scores, dist_scores] = getNMSRects(pm_search_semantic, target_sz_dat, cfg.nms_scale, ...
                cfg.nms_overlap, cfg.nms_score_factor, cos_win, cfg.nms_include_center_vote);
            candidate_scores = vote_scores .* dist_scores;
            [~, best_candidate] = max(candidate_scores);
            
            if p.visualization_dbg == 1
                figure(3), clf
                imagesc(pm_search_semantic,[0 1]);
                axis image; axis off;
                title('Search Window');
                for i = 1:size(hypotheses,1)
                    if i == best_candidate, color = 'r'; else color = 'y'; end
%                     color = [1 0 0.2];
                    rectangle('Position',hypotheses(i,:),'EdgeColor',color,'LineWidth',2);
                    text(hypotheses(i,1), hypotheses(i,2), [num2str(hypotheses(i,1)) ' ' num2str(hypotheses(i,2))], 'color', 'c');
                end
            end
        end
        
        [likelihood_map] = getColourMap(im_patch_pwp, bg_hist, fg_hist, p.n_bins, p.grayscale_sequence);
        % (TODO) in theory it should be at 0.5 (unseen colors should have max entropy)
        likelihood_map(isnan(likelihood_map)) = 0;
        
        % each pixel of response_pwp loosely represents the likelihood that
        % the target (of size norm_target_sz) is centred on it
        response_pwp = getCenterLikelihood(likelihood_map, p.norm_target_sz);
        
        [likelihood_map_search_win] = getColourMap(search_win, bg_hist, fg_hist, p.n_bins, p.grayscale_sequence);
        likelihood_map_search_win(padded_search_win) = 0;
        
        [likelihood_map_search_win_distractor] = getColourMap(search_win, distractor_hist, obj_hist, p.n_bins, p.grayscale_sequence);
        likelihood_map_search_win_distractor(padded_search_win) = 0;
        
        likelihood_map_search_win = .5 * likelihood_map_search_win + .5 * likelihood_map_search_win_distractor;
        
        % just for testing, output image
        [likelihood_map_search_im] = getColourMap(im, bg_hist, fg_hist, p.n_bins, p.grayscale_sequence);
        [likelihood_map_search_im_distractor] = getColourMap(im, distractor_hist, obj_hist, p.n_bins, p.grayscale_sequence);
        
        [pos, rect_position, response, distractors] = getPredictLoc(im, response_cf, response_pwp, pos, p, bg_area, hann_window, area_resize_factor, target_sz,...
            model_alphaf, model_xf, search_rect, hypotheses, vote_scores, dist_scores, pm_search_semantic, cos_win, likelihood_map_search_win, svm_distractor_w);
        
        %% scale adaption
        [pos, rect_position, CurrRes, target_color_score] = doScaleAdaption(im, likelihood_map, im_patch_pwp, im_patch_semantic, pos, rect_position, area_resize_factor,...
            grabCutParam, im_patch_pwp_rect, target_color_score, original_response_cf);

        %% SCALE SPACE SEARCH
        % use new scale to update bboxes for target, filter, bg and fg models
        target_sz = rect_position(3:4);
        target_sz = target_sz([2,1]);
        avg_dim = sum(target_sz)/2;
        bg_area = round(target_sz + avg_dim);
        if(bg_area(2)>size(im,2)),  bg_area(2)=size(im,2)-1;    end
        if(bg_area(1)>size(im,1)),  bg_area(1)=size(im,1)-1;    end
        
        bg_area = bg_area - mod(bg_area - target_sz, 2);
        fg_area = round(target_sz - avg_dim * p.inner_padding);
        fg_area = fg_area + mod(bg_area - fg_area, 2);
        % Compute the rectangle with (or close to) params.fixed_area and
        % same aspect ratio as the target bboxgetScaleSubwindow
        area_resize_factor = sqrt(p.fixed_area/prod(bg_area));

        if p.visualization_dbg==1
            mySubplot(2,2,4,1,im_patch_cf,'FG+BG','gray');
            mySubplot(2,2,4,2,likelihood_map,'obj.likelihood','parula');
            mySubplot(2,2,4,3,search_win, 'proposal search win', 'gray');
            mySubplot(2,2,4,4,likelihood_map_search_win, 'proposal likelihood', 'parula');
            mySubplot(2,2,4,5,response_cf,'CF response','parula');
            mySubplot(2,2,4,6,response_pwp,'center likelihood','parula');
            mySubplot(2,2,4,7,response,'merged response','parula');
            mySubplot(2,2,4,8,CurrRes,'segment result','gray');
            drawnow
        end
    end
    
    %% TRAINING
    % extract patch of size bg_area and resize to norm_bg_area
    im_patch_bg = getSubwindow(im, pos, p.norm_bg_area, bg_area);
    % compute feature map, of cf_response_size
    xt = getFeatureMap(im_patch_bg, p.feature_type, p.cf_response_size, p.hog_cell_size);
    % apply Hann window
    xt = bsxfun(@times, xt, hann_window);
    % compute FFT
    xf = fft2(xt);
    %Kernel Ridge Regression, calculate alphas (in Fourier domain)
    kf = gaussian_correlation(xf, xf, p.sigma);
    %% FILTER UPDATE
    % Compute expectations over circular shifts,
    % therefore divide by number of pixels.
    new_alphaf_num = yf .* kf;
    new_alphaf_den = kf .* (kf + p.lambda);
    
    if frame == 1
        % first frame, train with a single image
        alphaf_num = new_alphaf_num;
        alphaf_den = new_alphaf_den;
        model_xf = xf;
    else
        % subsequent frames, update the model by linear interpolation
        alphaf_num = (1 - p.learning_rate_cf) * alphaf_num + p.learning_rate_cf * new_alphaf_num;
        alphaf_den = (1 - p.learning_rate_cf) * alphaf_den + p.learning_rate_cf * new_alphaf_den;
        model_xf = (1 - p.learning_rate_cf) * model_xf + p.learning_rate_cf * xf;
        
        %% BG/FG MODEL UPDATE
        % patch of the target + padding
        [bg_hist, fg_hist] = updateHistModel(is_new_pwp_model, im_patch_bg, bg_area, fg_area, target_sz, p.norm_bg_area, p.n_bins, p.grayscale_sequence, bg_hist, fg_hist, p.learning_rate_pwp);
        
        search_win_sz = size(search_win);
        search_win_sz = search_win_sz(1:2);
        norm_area = round(search_win_sz * area_resize_factor);
        
        if size(distractors,1) > 1
            [distractor_hist, obj_hist] = updateDistractorHistModel(is_new_dist_model, search_win, distractors, rect_position, norm_area, p.n_bins, p.grayscale_sequence, distractor_hist, obj_hist, p.learning_rate_pwp);
            is_new_dist_model = 0;
        else
             % If there are no distractors, trigger decay of distractor histgorm
            distractor_hist = (1 - p.learning_rate_pwp)*distractor_hist + p.learning_rate_pwp*bg_hist;
            obj_hist = (1 - p.learning_rate_pwp)*obj_hist + p.learning_rate_pwp*fg_hist;
        end
        
        % use kcf location
        target_pos_dat = rect_position(1:2) + round(rect_position(3:4)/2);
        target_sz_dat = rect_position(3:4);
        
        state.target_pos_history = [state.target_pos_history; target_pos_dat];
        state.target_sz_history = [state.target_sz_history; target_sz_dat];
        
    end
    model_alphaf = alphaf_num ./ alphaf_den;
    
    % update bbox position
    if frame==1, rect_position = [pos([2,1]) - target_sz([2,1])/2, target_sz([2,1])]; end
    
    rect_position_padded = [pos([2,1]) - bg_area([2,1])/2, bg_area([2,1])];
    rect_position_padded_search = [pos([2,1]) - search_sz/2, search_sz];
    OTB_rect_positions(frame,:) = rect_position;
    
    if p.fout > 0,  fprintf(p.fout,'%.2f,%.2f,%.2f,%.2f\n', rect_position(1),rect_position(2),rect_position(3),rect_position(4));   end
    
    %% VISUALIZATION
    if p.visualization == 1
        if isToolboxAvailable('Computer Vision System Toolbox')
            im = insertShape(im, 'Rectangle', rect_position, 'LineWidth', 4, 'Color', 'yellow');
            im = insertShape(im, 'Rectangle', rect_position_padded, 'LineWidth', 4, 'Color', 'blue');
            im = insertShape(im, 'Rectangle', rect_position_padded_search, 'LineWidth', 4, 'Color', 'm');
            % Display the annotated video frame using the video player object.
            step(p.videoPlayer, im);
        else
            figure(1)
            imshow(im)
            rectangle('Position',rect_position, 'LineWidth',2, 'EdgeColor','y');
            rectangle('Position',rect_position_padded, 'LineWidth',2, 'LineStyle','--', 'EdgeColor','b');
            rectangle('Position',rect_position_padded_search, 'LineWidth',2, 'LineStyle','--', 'EdgeColor','m');
            drawnow
        end
    end
    
    if is_save_visual_result ==1
        img_fig = figure(5);
        clf;
        imshow(im, 'border','tight'); hold on;
        img_cols = size(im,2); img_rows = size(im,1);
        position_c = img_cols/3; position_r = img_rows/12;
        frmNo = str2num(p.img_files{frame}(regexp(p.img_files{frame},'0'):regexp(p.img_files{frame},'\.')-1));
        frmNo_str = sprintf('%04d', frmNo);
        text(img_cols - position_c, position_r, ['#',frmNo_str], 'Color', 'y', 'FontWeight','bold', ...
            'FontSize', img_cols/17);
        rectangle('Position',rect_position, 'LineWidth', 5, 'EdgeColor','y');
        drawnow
        
        name = ['figs/visual/', p.seq_name, '/', 'sat_', p.seq_name, '_', frmNo_str, '.jpg'];
        exportfig(img_fig, name, 'Format', 'jpeg', 'Color', 'rgb');
    end
end
elapsed_time = toc;
% save data for OTB-13 benchmark
results.type = 'rect';
results.res = OTB_rect_positions;
results.fps = num_frames/(elapsed_time - t_imread);
end

% Reimplementation of Hann window (in case signal processing toolbox is missing)
function H = myHann(X)
H = .5*(1 - cos(2*pi*(0:X-1)'/(X-1)));
end

% We want odd regions so that the central pixel can be exact
function y = floor_odd(x)
y = 2*floor((x-1) / 2) + 1;
end