
% load path
addpath('correlation_filter/');
addpath('params/');
addpath('probability/');
addpath('utils/');
addpath('semantic/');
addpath('dat/');
addpath('grabcut/');
addpath('grabcut/GCMex/');

dir_matConvNet='/media/rui/study/deeplearning/matconvnet-1.0-beta23/matlab/';
run([dir_matConvNet 'vl_setupnn.m']);

semantic_path = '../../data/semantic_label';
seq_name = 'Tiger1'; 
sequence_path = '../../data/OTB/Datasets';
dataset = 'OTB'; 

runTracker(seq_name, sequence_path, dataset, semantic_path);