function runTracker(sequence, sequence_path, dataset, semantic_path, start_frame)
% RUN_TRACKER  is the external function of the tracker - does initialization and calls trackerMain

%% Read parameters
params = readParams('params.txt');

% Load default settings
cfg = default_parameters_dat();

%% load video info
switch dataset
    case 'OTB' 
        [params.bb, img_path, img_files] = getOtbSeqInfo(sequence_path, sequence);
    case 'VOT15'
        [params.bb, img_path, img_files] = getVotSeqInfo(sequence_path, sequence);
    otherwise
        [params.bb, img_path, img_files] = getDemoSeqInfo(sequence_path, sequence);
end

im = imread([img_path img_files{1}]);
% is a grayscale sequence ?
if(size(im,3)==1)
    params.grayscale_sequence = true;
end

params.img_files = img_files;
params.img_path = img_path;
params.seq_name = sequence;
params.dataset = dataset;
params.semantic_path = semantic_path;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
region = params.bb(1,:);
if(numel(region)==8)
    % polygon format
    [cx, cy, w, h] = getAxisAlignedBB(region);
else
    x = region(1);
    y = region(2);
    w = region(3);
    h = region(4);
    cx = x+w/2;
    cy = y+h/2;
end

% init_pos is the centre of the initial bounding box
params.init_pos = [cy cx];
params.target_sz = round([h w]);
[params, bg_area, fg_area, area_resize_factor] = initializeAllAreas(im, params);
if params.visualization
    params.videoPlayer = vision.VideoPlayer('Position', [100 100 [size(im,2), size(im,1)]+30]);
end
% in runTracker we do not output anything
params.fout = -1;

% initialize svm for distractors
% svm_distractor_w = initialiseDistractorSVM(im, region, params);
svm_distractor_w = [];

% start the actual tracking
results = trackerMain(sequence_path, params, im, bg_area, fg_area, area_resize_factor, cfg, svm_distractor_w);
fprintf('Average speed %3.2f (fps).\n', results.fps);
fclose('all');

end

%%
function [bb, img_path, img_files] = getOtbSeqInfo(sequence_path, sequence, start_frame)

sequence_path = [sequence_path,'/', sequence,'/'];
img_path = [sequence_path, 'img/'];

% get ground truth
bb = csvread([sequence_path 'groundtruth_rect.txt']);

if exist('start_frame')
    frames = start_frame;
else
    frames = 1;
end
start_frame = frames;

% read all the frames in the 'imgs' subfolder
dir_content = dir([sequence_path, '/img/']);
% skip '.' and '..' from the count
n_imgs = length(dir_content) - 2;
img_files = cell(n_imgs, 1);
for ii = 1:n_imgs
    img_files{ii} = dir_content(ii+2).name;
end
img_files(1:start_frame-1)=[];

end

%%
function [bb, img_path, img_files] = getVotSeqInfo(sequence_path, sequence, start_frame)

sequence_path = [sequence_path,'/', sequence,'/'];
img_path = [sequence_path, '/'];

% get ground truth
bb = csvread([sequence_path 'groundtruth.txt']);

if exist('start_frame')
    frames = start_frame;
else
    frames = 1;
end
start_frame = frames;

% read all the frames in the 'imgs' subfolder
dir_content = dir([sequence_path, '/*.jpg']);
n_imgs = length(dir_content);
img_files = cell(n_imgs, 1);
for ii = 1:n_imgs
    img_files{ii} = dir_content(ii).name;
end
img_files(1:start_frame-1)=[];

end

%%
function [bb, img_path, img_files] = getDemoSeqInfo(sequence_path, sequence, start_frame)

sequence_path = [sequence_path,'/', sequence,'/'];
img_path = [sequence_path, 'imgs/'];
% Read files
text_files = dir([sequence_path '/*_frames.txt']);
f = fopen([sequence_path text_files(1).name]);
frames = textscan(f, '%f,%f');
if exist('start_frame')
    frames{1} = start_frame;
else
    frames{1} = 1;
end
start_frame = frames{1};
fclose(f);

%     params.bb_VOT = csvread([sequence_path 'groundtruth.txt']);
gt_files = dir([sequence_path '/*_gt.txt']);
%     params.bb_VOT = csvread([sequence_path '\' sequence '_gt.txt']);
bb = csvread([sequence_path gt_files(1).name]);

% read all the frames in the 'imgs' subfolder
dir_content = dir([sequence_path, '/imgs/']);
% skip '.' and '..' from the count
n_imgs = length(dir_content) - 2;
img_files = cell(n_imgs, 1);
for ii = 1:n_imgs
    img_files{ii} = dir_content(ii+2).name;
end

img_files(1:start_frame-1)=[];

end
