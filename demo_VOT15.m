
% load path
addpath('correlation_filter/');
addpath('params/');
addpath('probability/');
addpath('utils/');
addpath('semantic/');
addpath('dat/');
addpath('grabcut/');
addpath('grabcut/GCMex/');
% addpath('svm/');
% addpath('grabcut/struct-edge-detector/');

dir_matConvNet='/media/rui/study/deeplearning/matconvnet-1.0-beta23/matlab/';
run([dir_matConvNet 'vl_setupnn.m']);

semantic_path = '../../data/semantic_label';

seq_name = 'road'; 
sequence_path = '../../data/VOT/VOT15';
dataset = 'VOT15'; 

runTracker(seq_name, sequence_path, dataset, semantic_path);