function [ im_semantic, layers ] = get_semantic_map( s_scoremap, im, pos, model_sz, scaled_sz, layers )

% with 3 input, no scale. With 4 params, scale adaptation
if nargin < 4, sz = model_sz;   
else, sz = scaled_sz;    
end

%make sure the size is not to small
sz = max(sz, 2);
xs = round(pos(2) + (1:sz(2)) - sz(2)/2);
ys = round(pos(1) + (1:sz(1)) - sz(1)/2);

%check for out-of-bounds coordinates, and s et them to the values at
%the borders
xs(xs < 1) = 1;
ys(ys < 1) = 1;
xs(xs > size(im,2)) = size(im,2);
ys(ys > size(im,1)) = size(im,1);

scoremap = s_scoremap.data_obj.score_map(:,:,1:end);
scoremap_resample = zeros(s_scoremap.data_obj.img_size(1),s_scoremap.data_obj.img_size(2),size(scoremap,3));
if layers(1) < 0
    % get subwindows 
    for i = 2:size(scoremap,3) % 1: use background, 2: unuse background
        scoremap_resample(:,:,i) = imResample(scoremap(:,:,i), size(im));
    end
    
    % get largest n layers
    n_layers = abs(layers(1));
    patch_scoremap = scoremap_resample(ys,xs,:);
    s_sum = sum(reshape(patch_scoremap, [], size(patch_scoremap, 3)));
    [values, originalpos] = sort( s_sum, 'descend' );
    layers = originalpos(1:n_layers);
end

%% Resample with feature_sz
im_semantic = zeros(size(im, 1), size(im, 2), size(layers, 2));
for i = 1:size(layers,2)
    im_semantic(:,:,i) = imResample(scoremap(:,:,layers(i)), size(im));
end

im_semantic = uint8(sum(im_semantic, 3)/size(layers,2));

end


