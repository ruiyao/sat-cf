function [accumulate_scoremap, patch_semantic, semantic] = getSemanticSubwindow(s_scoremap, im, pos, model_sz, scaled_sz, target_sz, semantic)

% with 3 input, no scale. With 4 params, scale adaptation
if nargin < 4, sz = model_sz;   
else, sz = scaled_sz;    
end

% pre_semantic = semantic;
% n_keep_score = 8;

%make sure the size is not to small
sz = max(sz, 2);
xs = round(pos(2) + (1:sz(2)) - sz(2)/2);
ys = round(pos(1) + (1:sz(1)) - sz(1)/2);

%check for out-of-bounds coordinates, and set them to the values at
%the borders
xs(xs < 1) = 1;
ys(ys < 1) = 1;
xs(xs > size(im,2)) = size(im,2);
ys(ys > size(im,1)) = size(im,1);

scoremap = s_scoremap.data_obj.score_map(:,:,1:end);
scoremap_resample = zeros(s_scoremap.data_obj.img_size(1),s_scoremap.data_obj.img_size(2),size(scoremap,3));

% get target area
t_sz = max(target_sz, 2);
target_xs = round(pos(2) + (1:t_sz(2)) - t_sz(2)/2);
target_ys = round(pos(1) + (1:t_sz(1)) - t_sz(1)/2);

%check for out-of-bounds coordinates, and set them to the values at
%the borders
target_xs(target_xs < 1) = 1;
target_ys(target_ys < 1) = 1;
target_xs(target_xs > size(im,2)) = size(im,2);
target_ys(target_ys > size(im,1)) = size(im,1);

is_frist_frame = 0;

n_layers = size(semantic.layers, 2);
if semantic.layers(1) < 0
    % get subwindows 
    for i = 2:size(scoremap,3) % 1: use background, 2: unuse background
        scoremap_resample(:,:,i) = imResample(scoremap(:,:,i), size(im));
    end
    is_frist_frame = 1;
    
    % get the largest n layers
    n_layers = abs(semantic.layers(1));
    patch_scoremap = scoremap_resample(ys,xs,:);
    s_sum = sum(reshape(patch_scoremap, [], size(patch_scoremap, 3)));
    [values, originalpos] = sort( s_sum, 'descend' );
    semantic.layers = originalpos(1:n_layers);
    
    tmp = scoremap_resample(target_ys,target_xs,semantic.layers(1));
    avg_score = sum(tmp(:))/(size(tmp,1)*size(tmp,2));
    
    if avg_score > 50
        n_layers = 1;
    end
   
    semantic.layers = semantic.layers(1:n_layers);
end

%% Resample with feature_sz
% patch_semantic = zeros(model_sz(1), model_sz(2), n_layers);
tmp_score = 0;
accumulate_scoremap = uint8(zeros(size(im, 1), size(im, 2)));
for i = 1:n_layers
    tmp_im = imResample(scoremap(:,:,semantic.layers(i)), size(im));
    accumulate_scoremap = accumulate_scoremap + tmp_im;
    
    % 1.2 target regions
    % get target area
    t_sz_padding = round(max(target_sz, 2)*1.2);
    target_padding_xs = round(pos(2) + (1:t_sz_padding(2)) - t_sz_padding(2)/2);
    target_padding_ys = round(pos(1) + (1:t_sz_padding(1)) - t_sz_padding(1)/2);

    %check for out-of-bounds coordinates, and set them to the values at
    %the borders
    target_padding_xs(target_padding_xs < 1) = 1;
    target_padding_ys(target_padding_ys < 1) = 1;
    target_padding_xs(target_padding_xs > size(im,2)) = size(im,2);
    target_padding_ys(target_padding_ys > size(im,1)) = size(im,1);
    
    try
        tmp_target = tmp_im(target_padding_ys, target_padding_xs);
        tmp_score = tmp_score + sum(tmp_target(:))/(size(tmp_target,1)*size(tmp_target,2));
    catch
        keyboard
    end
end

tmp_bg = accumulate_scoremap(ys, xs);

motionmap=mat2gray(tmp_bg);
motionmap=(motionmap - 0.5)*2;
if n_layers < 3
    if ~is_frist_frame
    %     tic
        temp = vl_nnpool(semantic.patch_semantic, 3); % 3: max pooling
    %     toc
        temp_padding = zeros(size(motionmap));

        tmp_width = size(temp, 1);
        tmp_height = size(temp, 2);
        if size(motionmap, 1) < tmp_width
            tmp_width = size(motionmap, 1);
        end
        if size(motionmap, 2) < tmp_height
            tmp_height = size(motionmap, 2);
        end

        temp_padding(1:tmp_width, 1:tmp_height) = temp(1:tmp_width, 1:tmp_height);
        temp_padding(temp_padding<0) = 0; % max(u_{t-1}(q), 0)
        try
        max_path_score1 = temp_padding + motionmap;
        catch 
            keyboard;
        end
        max_path_score2 = (max_path_score1 + 1)/2; % change to [0 1]

        semantic.patch_semantic = motionmap .* max_path_score2;
    else
        semantic.patch_semantic = motionmap;
    end
else
    semantic.patch_semantic = tmp_bg;
end
patch_semantic = mexResize(semantic.patch_semantic, model_sz, 'auto');
patch_semantic = round((255-0)*(patch_semantic+1)/(1+1) + 0);

% patch_semantic = tmp_bg;

end
