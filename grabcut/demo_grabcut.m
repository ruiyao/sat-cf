
addpath 'struct-edge-detector';
addpath 'GCMex';

% read image
im = imread('image/test1.jpg');
imd = double(im);

fixedBG = ones(size(im,1), size(im,2));
col = 31;
row = 41;
end_col = size(im, 1);
end_row = size(im, 2);

fixedBG(col:end_col, row:end_row) = 0;
fixedBG = logical(fixedBG);

% parameter
Beta = 0.3;
k = 6;
G = 50;
maxIter = 10;
diffThreshold = 0.001;

% call the grab cut algorithm
warning off;
L = GCAlgo(imd, fixedBG, k, G, maxIter, Beta, diffThreshold);
L = double(1 - L);
warning on;

CurrRes = imd.*repmat(L , [1 1 3]);

figure; imshow(im); hold on;
fore_rect = [ row col  end_row-row end_col-col];
rectangle('Position',fore_rect,'EdgeColor','b','LineWidth',2);
hold off;

figure;imshow(uint8(CurrRes));

