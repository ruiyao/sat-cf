function param = loadGrabCutParam
%LOADGRABCUTPARA Summary of this function goes here
%   Detailed explanation goes here

param.Beta = 0.3;
param.k = 6;
param.G = 1;
param.maxIter = 1;
param.diffThreshold = 0.001;

end

