
% load path
addpath('correlation_filter/');
addpath('params/');
addpath('probability/');
addpath('utils/');
addpath('semantic/');
addpath('dat/');
addpath('grabcut/');
addpath('grabcut/GCMex/');

dir_matConvNet = 'matconvnet/matconvnet-1.0-beta23/matlab/';
run([dir_matConvNet 'vl_setupnn.m']);

semantic_path = 'sequences/semantic_label';
seq_name = 'BlurCar4'; 
sequence_path = 'sequences/image';
dataset = 'demo'; 

runTracker(seq_name, sequence_path, dataset, semantic_path);