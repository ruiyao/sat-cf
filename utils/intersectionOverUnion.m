function iou = intersectionOverUnion(target_rect, candidates)
assert(size(target_rect,1) == 1)
inA = rectint(candidates,target_rect);
unA = prod(target_rect(3:4)) + prod(candidates(:,3:4),2) - inA;
iou = inA ./ max(eps,unA);
end