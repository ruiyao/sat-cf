function [pos_scale, rect_position_scale, CurrRes, target_color_score] = doScaleAdaption(im, likelihood_map, im_patch_pwp, im_patch_semantic,...
    pos, rect_position, area_resize_factor, grabCutParam, im_patch_pwp_rect, pre_target_color_score, original_response_cf)

%% prepare foreground
pre_pos = pos;
pos = pos([2,1]);
pre_target_sz_win = rect_position(3:4) .* area_resize_factor;

pos_resize = round((pos - im_patch_pwp_rect(1:2)).* area_resize_factor);
rect_position_resize = rect_position;

rect_position_resize(3:4) = round(rect_position_resize(3:4) .* area_resize_factor .* 1.5);
rect_position_resize(1:2) = round(pos_resize - rect_position_resize(3:4) / 2);

if rect_position_resize(1) < 1, rect_position_resize(1) = 1; end
if rect_position_resize(2) < 1, rect_position_resize(2) = 1; end
if rect_position_resize(1)+rect_position_resize(3) > size(im_patch_pwp, 2)
    rect_position_resize(3) = size(im_patch_pwp, 2)-rect_position_resize(1);
end
if rect_position_resize(2)+rect_position_resize(4) > size(im_patch_pwp, 1)
    rect_position_resize(4) = size(im_patch_pwp, 1)-rect_position_resize(2);
end

%% set background
imd = double(im_patch_pwp);
% figure;imshow(im_patch_pwp);
fixedBG = ones(size(imd,1), size(imd,2));
try
    fixedBG(rect_position_resize(2):rect_position_resize(2)+rect_position_resize(4), rect_position_resize(1):rect_position_resize(1)+rect_position_resize(3)) = 0;
catch
    keyboard;
end
fixedBG = logical(fixedBG);
prevLabel = double(fixedBG);

% figure;imshow(fixedBG);

%% calculate the smoothness term defined by the entire image's RGB values
bNormGrad = true;

gradH = im_patch_pwp(:,2:end,:) - im_patch_pwp(:,1:end-1,:);
gradV = im_patch_pwp(2:end,:,:) - im_patch_pwp(1:end-1,:,:);

gradH = sum(gradH.^2, 3);
gradV = sum(gradV.^2, 3);

%%% Use the gradient to calculate the graph's inter-pixels weights
if ( bNormGrad )
    hC = exp(-grabCutParam.Beta.*gradH./mean(gradH(:)));
    vC = exp(-grabCutParam.Beta.*gradV./mean(gradV(:)));
else
    hC = exp(-grabCutParam.Beta.*gradH);
    vC = exp(-grabCutParam.Beta.*gradV);
end

%%% These matrices will evantually use as inputs to Bagon's code
hC = [hC zeros(size(hC,1),1)];
vC = [vC ; zeros(1, size(vC,2))];
sc = [0 grabCutParam.G; grabCutParam.G 0];

%% get correlation filter response for unary term
patch_sz = size(im_patch_pwp);
patch_sz = patch_sz(1:2);
response_cf = mexResize(original_response_cf, patch_sz,'auto');

%% calculate the data term

% im_patch_semantic = mat2gray(im_patch_semantic);
semantic_mask = zeros(size(im_patch_semantic, 1), size(im_patch_semantic, 2));
semantic_mask(im_patch_semantic>=1) = 1;

likelihood_map_merge = 0.7 * likelihood_map + 0.3 * response_cf;

bgLogPL = likelihood_map_merge;
fgLogPL = 1-likelihood_map_merge;

%% grabcut
% call the grab cut algorithm
% try
for i = 1:1
    warning off;
    fgLogPL(fixedBG) = max(max(fgLogPL));
    
    %%% Now that we have all inputs, calculate the min-cut of the graph
    %%% using Bagon's code. Not much to explain here, for more details read
    %%% the graph cut documentation in the   GraphCut.m    file.
    % tic;
    dc = cat(3, bgLogPL, fgLogPL);
    % dc = CalcClusterCost(imd);
    
    graphHandle = GraphCut('open', dc , sc, vC, hC);
    graphHandle = GraphCut('set', graphHandle, int32(prevLabel == 0));
    [graphHandle currLabel] = GraphCut('expand', graphHandle);
    currLabel = 1 - currLabel;
    GraphCut('close', graphHandle);
    prevLabel = currLabel;
end
% catch message
%     keyboard;
% end
warning on;
% toc;

L = double(1 - currLabel);

% try
tmp_st = regionprops(L, 'BoundingBox' );

if ~isempty(tmp_st)
    
    tmp_rect_st = round(tmp_st.BoundingBox(1,:));
    % catch
    %     keyboard;
    % end
    
    tmp_rect_st_col = tmp_rect_st(1) + tmp_rect_st(3)/2;
    tmp_rect_st_row = tmp_rect_st(2) + tmp_rect_st(4)/2;
    tmp_color_score = getAvgLikelihood([tmp_rect_st_col tmp_rect_st_row], [tmp_rect_st(3) tmp_rect_st(4)], semantic_mask);
    
    if tmp_color_score > 0.3
        L = L .* semantic_mask;
    end
    
    st = regionprops(L, 'BoundingBox' );
    rect_st = round(st.BoundingBox(1,:));
    
    width_scale = rect_st(3) / pre_target_sz_win(1);
    height_scale = rect_st(4) / pre_target_sz_win(2);
    
    % scale_resonable_ratio_max = 1.1;
    scale_resonable_ratio_min = 0.8;
    scale_factor = 0.3;
    
    tmp_col = round(rect_st(1)+rect_st(3)/2);
    tmp_row = round(rect_st(2)+rect_st(4)/2);
    
    proposal_color_score = getAvgLikelihood([tmp_col tmp_row], [rect_st(3) rect_st(4)], L);
    color_score_ratio = 0.6;
    
    if (width_scale < scale_resonable_ratio_min) || ((proposal_color_score < color_score_ratio))
        new_width = pre_target_sz_win(1);
    else
        new_width = round(scale_factor * rect_st(3) + (1-scale_factor) * pre_target_sz_win(1));
    end
    if (height_scale < scale_resonable_ratio_min) || ((proposal_color_score < color_score_ratio))
        %     height_scale
        new_height = pre_target_sz_win(2);
    else
        new_height = round(scale_factor * rect_st(4) + (1-scale_factor) * pre_target_sz_win(2));
    end
    
    scale_factor_shrink = 0.25;
    if (width_scale < 1)
        new_width = round(scale_factor_shrink * rect_st(3) + (1-scale_factor_shrink) * pre_target_sz_win(1));
    end
    if (height_scale < 1)
        new_height = round(scale_factor_shrink * rect_st(4) + (1-scale_factor_shrink) * pre_target_sz_win(2));
    end
    
    new_width = round(new_width / area_resize_factor);
    new_height = round(new_height / area_resize_factor);
    
    new_row = round(pos(2)-new_height/2);
    new_col = round(pos(1)-new_width/2);
    % new_row = round(tmp_row/area_resize_factor + im_patch_pwp_rect(2) - new_height/2);
    % new_col = round(tmp_col/area_resize_factor + im_patch_pwp_rect(1) - new_width/2);
    
    if new_row < 1, new_row = 1; end
    if new_col < 1, new_col = 1; end
    
    if new_row+new_height > size(im, 1)
        new_height = size(im, 1) - new_row;
    end
    if new_col+new_width > size(im, 2)
        new_width = size(im, 2) - new_col;
    end
    
    pos_scale = [round(new_row+new_height/2) round(new_col+new_width/2)];
    rect_position_scale = [new_col new_row new_width new_height];
    
    curr_pos_win = round((pos_scale - im_patch_pwp_rect([2,1]))*area_resize_factor);
    target_color_score = getAvgLikelihood(curr_pos_win([2,1]), [new_width new_height], likelihood_map_merge);
    
    if ~ismatrix(imd)
        CurrRes = uint8(imd.*repmat(L , [1 1 3]));
    else
        CurrRes = uint8(imd.*repmat(L , [1 1 1]));
    end
    
    
else
    pos_scale = pre_pos;
    rect_position_scale = rect_position;
    CurrRes = uint8(im_patch_pwp);
    target_color_score = pre_target_color_score;
end

%
% figure; imshow(im_patch_pwp); hold on;
% % fore_rect = [ rect_position_resize(1) rect_position_resize(2)  rect_position_resize(2) rect_position_resize(2)];
% rectangle('Position',rect_position_resize,'EdgeColor','b','LineWidth',2);
% rectangle('Position',rect_st,'EdgeColor','r','LineWidth',2);
% hold off;
%
% figure; imshow(im); hold on;
% rectangle('Position',rect_position_scale,'EdgeColor','y','LineWidth',2);
% figure;imshow(uint8(CurrRes));


end

%%
function avg_likelihood = getAvgLikelihood(center_pos, sz, likelihoodmap)

[out, mask] = getSubwindowMasked(likelihoodmap, center_pos, sz);
out(mask) = 0;
out(isnan(out)) = 0;
avg_likelihood = sum(out(:)) / prod(sz);

end