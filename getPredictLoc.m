function [pos, rect_position, response, distractors] = getPredictLoc(im, response_cf, response_pwp, pos, p, bg_area, hann_window, area_resize_factor, target_sz,...
    model_alphaf, model_xf, search_rect, hypotheses, vote_scores, dist_scores, pm_search_semantic, cos_win, likelihood_map_search_win, svm_distractor_w)

distractors = [];

%% get response of correlation filter and pixel-wise color histogram probability
response = mergeResponses(response_cf, response_pwp, p.merge_factor, p.merge_method);

max_response_cf = max(response_cf(:));
[row, col] = find(response == max(response(:)), 1);
center = (1+p.norm_delta_area) / 2;
pos = pos + ([row, col] - center) / area_resize_factor;

pos_win = round(pos([2,1]) - search_rect(1:2) + 1);
color_score = getAvgLikelihood(pos_win, target_sz([2,1]), likelihood_map_search_win);
max_response_cf_color = max_response_cf * (1-p.merge_factor_color) + color_score * p.merge_factor_color;

semantic_score = getAvgLikelihood(pos_win, target_sz([2,1]), pm_search_semantic);

rect_position = [pos([2,1]) - target_sz([2,1])/2, target_sz([2,1])];
max_rect = rect_position;
max_pos = pos;

%% semantic proposals
if ~isempty(hypotheses)
candidate_centers = hypotheses(:,1:2) + hypotheses(:,3:4)./2;

for i = 1:size(candidate_centers, 1)
	% get proposal location on image based on colol histogram probability
    proposal_pos_img_original = round(candidate_centers(i, :) + search_rect(1:2) - 1);
    proposal_rect_position_original = [proposal_pos_img_original - target_sz([2,1])/2, target_sz([2,1])];
    
    % ingore candidate proposals large than overlap ratio
    proposal_rect = [hypotheses(i, 1:2) + search_rect(1:2) - 1 hypotheses(i, 3:4)];
    
    %% fixed size of bounding box
    % get subwindow and feature
    im_patch_cf = getSubwindow(im, proposal_pos_img_original([2,1]), p.norm_bg_area, bg_area);

    %% get response
    % compute feature map
    xt = getFeatureMap(im_patch_cf, p.feature_type, p.cf_response_size, p.hog_cell_size);

    % apply Hann window
    xt = bsxfun(@times, hann_window, xt);
    % compute FFT
    xf = fft2(xt);
    kzf = gaussian_correlation(xf, model_xf, p.sigma);
    tmp_response = real(ifft2(model_alphaf .* kzf));
    response_cf_proposal = max(tmp_response(:));
    
    map_dist_scores = cos_win(round(candidate_centers(i, 2)), round(candidate_centers(i, 1)));
    
    proposal_color_score = getAvgLikelihood(candidate_centers(i,:), hypotheses(i, 3:4), likelihood_map_search_win);
    
    %% replace correlation filter score with svm score
    response_proposal_color = response_cf_proposal * (1-p.merge_factor_color) + proposal_color_score * p.merge_factor_color;

    if (response_proposal_color > max_response_cf_color) && (map_dist_scores > 0.1)
        max_response_cf_color = response_proposal_color;
        max_rect = proposal_rect;
        max_pos = proposal_pos_img_original([2,1]);
    end
    
    overlap = intersectionOverUnion(rect_position, proposal_rect_position_original);
    if overlap < 0.1
        distractors = [distractors; hypotheses(i,:)];
    end

    if (semantic_score < 0.1) && (size(hypotheses, 1) == 1) && (map_dist_scores > 0.15) && (response_proposal_color > 0.3)
        max_rect = proposal_rect;
        max_pos = proposal_pos_img_original([2,1]);
    end

end
% fprintf('\n');
end

rect_position = max_rect;
pos = max_pos;

end

% target_rect Single 1x4 rect
function iou = intersectionOverUnion(target_rect, candidates)
assert(size(target_rect,1) == 1)
inA = rectint(candidates,target_rect);
unA = prod(target_rect(3:4)) + prod(candidates(:,3:4),2) - inA;
iou = inA ./ max(eps,unA);
end

function avg_likelihood = getAvgLikelihood(center_pos, sz, likelihoodmap)

[out, mask] = getSubwindowMasked(likelihoodmap, center_pos, sz);
out(mask) = 0;
out(isnan(out)) = 0;
avg_likelihood = sum(out(:)) / prod(sz);

end